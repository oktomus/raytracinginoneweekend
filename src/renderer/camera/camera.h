#ifndef RENDERER_CAMERA_CAMERA_H
#define RENDERER_CAMERA_CAMERA_H

#include "foundation/math/vector3.h"
#include "foundation/math/ray.h"

namespace renderer {

class Camera
{
  public:
    Camera(
		foundation::Vector3 	look_from,
		foundation::Vector3 	look_at,
		foundation::Vector3 	up,
		float 					vertical_fov, 
		float 					aspect,
		float					apperture,
		float 					focus_dist);

	foundation::Ray get_ray(float r, float s);

	foundation::Vector3 m_origin;
	foundation::Vector3 m_lower_left_corner;
	foundation::Vector3 m_horizontal;
	foundation::Vector3 m_vertical;
	foundation::Vector3 m_u, m_v, m_w;
	float m_lens_radius;
};

} // namespace renderer

#endif

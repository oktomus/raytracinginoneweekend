
// Associated header file
#include "renderer/camera/camera.h"

// Inner dependencies
#include "foundation/math/vector3.h"
#include "foundation/math/ray.h"

using namespace foundation;

namespace renderer
{

namespace
{

Vector3 random_in_unit_disk()
{
	Vector3 p;
	do
	{
		p = 2.0 * Vector3(drand48(), drand48(), 0) - Vector3(1, 1, 0);
	} while (Vector3::dot(p, p) >= 1.0);
	return p;
}

}

Camera::Camera(
    Vector3 	look_from,
    Vector3 	look_at,
    Vector3 	up,
    float 		vertical_fov, 
    float 		aspect,
    float 		apperture,
    float		focus_dist)
{
    m_lens_radius = apperture / 2;

    float theta = vertical_fov * M_PI / 180;
    float hheight = tan(theta / 2);
    float hwidth = aspect * hheight;

    m_origin = look_from;

    m_w = Vector3::normalize(look_from - look_at);
    m_u = Vector3::normalize(Vector3::cross(up, m_w));
    m_v = Vector3::cross(m_w, m_u);

    m_lower_left_corner = m_origin - hwidth * focus_dist * m_u - hheight * focus_dist * m_v - focus_dist * m_w;
    m_horizontal = 2 * hwidth * focus_dist * m_u;
    m_vertical = 2 * hheight * focus_dist * m_v;
}

Ray Camera::get_ray(float s, float t)
{
	Vector3 rd = m_lens_radius * random_in_unit_disk();
	Vector3 offset = m_u * rd.x + m_v * rd.y;
    return Ray(m_origin + offset, m_lower_left_corner + s * m_horizontal + t * m_vertical - m_origin - offset);
}


}; // Namespace renderer

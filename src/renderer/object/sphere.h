#ifndef RENDERER_OBJECT_SPHERE_H
#define RENDERER_OBJECT_SPHERE_H

#include "renderer/material/material.h"
#include "renderer/object/hitable.h"

using namespace foundation;

namespace renderer {

class Sphere : public Hitable
{
  public:
	Sphere(const foundation::Vector3 & center, float radius, Material * material);

	bool hit(const foundation::Ray& r, float t_min, float t_max, HitRecord& record) const override;

  private:
	foundation::Vector3 m_center;
	float m_radius;
	Material * m_material;
};

} // namespace renderer

#endif

#include "renderer/object/sphere.h"

using namespace foundation;

namespace renderer {

Sphere::Sphere(const Vector3 &center, float radius, Material *material):
    m_center(center), m_radius(radius), m_material(material)
{
}

bool Sphere::hit(const Ray &r, float t_min, float t_max, HitRecord &record) const
{
	// Ray to sphere
	Vector3 oc = r.origin() - m_center;
	// t * t * dot(B, B) + t * 2 * dot(B, A - C) + dot(A - C, A - C) - r * r = 0
	//         \   a   /       \      b        /   \       c               /
	// B : ray direction
	// A : ray origin
	// C : sphere center
	float a = Vector3::dot(r.direction(), r.direction());
	float b = Vector3::dot(r.direction(), oc);
	float c = Vector3::dot(oc, oc) - m_radius * m_radius;
	float delta = b * b - a * c;

	if (delta > 0)
	{
		float t = (-b - sqrt(delta)) / a;

		if(t < t_min || t > t_max)
            t = (-b + sqrt(delta)) / a;

		if (t > t_min && t < t_max)
		{
			record.t = t;
            record.p = r.point_at_parameter(record.t);
            record.normal = (record.p - m_center) / m_radius;
			record.mat_ptr = m_material;
			return true;
		}
	}

	return false;
}

} // namespace renderer

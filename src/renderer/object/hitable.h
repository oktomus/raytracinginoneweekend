#ifndef RENDERER_OBJECT_HITABLE_H
#define RENDERER_OBJECT_HITABLE_H

#include "foundation/math/ray.h"
#include "foundation/math/vector3.h"

using namespace foundation;

namespace renderer {

class Material;

class HitRecord
{
  public:
	float t;
	foundation::Vector3 p;
	foundation::Vector3 normal;
	Material * mat_ptr;
};

class Hitable
{
  public:
	virtual bool hit(const foundation::Ray& r, float t_min, float t_max, HitRecord& record) const = 0;
};

} // namespace renderer

#endif

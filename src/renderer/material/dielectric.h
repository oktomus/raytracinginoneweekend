#ifndef RENDERER_MATERIAL_DIELECTRIC_H
#define RENDERER_MATERIAL_DIELECTRIC_H

#include "renderer/material/material.h"

namespace renderer {

class Dielectric : public Material
{
  public:
	Dielectric(const float& ri);

	bool scatter(
        const Ray& ray_in,
        const HitRecord& hit_rec,
        Vector3& attenuation,
	    Ray& ray_scattered) const override;

	float ref_idx;
};

} // namespace renderer

#endif

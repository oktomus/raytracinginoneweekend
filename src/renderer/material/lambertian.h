#ifndef RENDERER_MATERIAL_LAMBERTIAN_H
#define RENDERER_MATERIAL_LAMBERTIAN_H

#include "renderer/material/material.h"

namespace renderer {

class Lambertian : public Material
{
  public:
	Lambertian(const Vector3& al);

	bool scatter(
        const Ray& ray_in,
        const HitRecord& hit_rec,
        Vector3& attenuation,
	    Ray& ray_scattered) const override;

	Vector3 albedo;
};

} // namespace renderer

#endif

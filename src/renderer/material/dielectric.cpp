#include "renderer/material/dielectric.h"

using namespace foundation;

namespace renderer {

Dielectric::Dielectric(const float &ri) :
    ref_idx(ri)
{
}

bool Dielectric::scatter(
    const Ray &ray_in,
    const HitRecord &hit_rec,
    Vector3 &attenuation,
    Ray &ray_scattered) const
{
	Vector3 out_normal;
	Vector3 reflected = reflect(ray_in.direction(), hit_rec.normal);
	float ni_over_nt;
	attenuation = Vector3(1, 1, 1);
	Vector3 refracted;
	float reflect_prob;
	float cosine;

	if (Vector3::dot(ray_in.direction(), hit_rec.normal) > 0)
	{
		out_normal = -hit_rec.normal;
		ni_over_nt = ref_idx;
		cosine = ref_idx * Vector3::dot(ray_in.direction(), hit_rec.normal) / ray_in.direction().length();
	}
	else
	{
		out_normal = hit_rec.normal;
		ni_over_nt = 1.0f / ref_idx;
		cosine = -Vector3::dot(ray_in.direction(), hit_rec.normal) / ray_in.direction().length();
	}

	if (refract(ray_in.direction(), out_normal, ni_over_nt, refracted))
	{
		reflect_prob = schlick(cosine, ref_idx);
	}
	else
	{
		ray_scattered = Ray(hit_rec.p, reflected);
		reflect_prob = 1.0f;
	}

	if (drand48() < reflect_prob)
		ray_scattered = Ray(hit_rec.p, reflected);
	else
		ray_scattered = Ray(hit_rec.p, refracted);

	return true;
}

} // namespace renderer

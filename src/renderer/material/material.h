#ifndef RENDERER_MATERIAL_MATERIAL_H
#define RENDERER_MATERIAL_MATERIAL_H

#include "foundation/math/vector3.h"
#include "foundation/math/ray.h"

#include "renderer/object/hitable.h"

using namespace foundation;

namespace renderer {

class Material
{
  public:
	virtual bool scatter(
        const Ray& ray_in,
        const HitRecord& hit_rec,
        Vector3& attenuation,
	    Ray& ray_scattered) const = 0;
};

Vector3 random_in_unit_sphere();

Vector3 reflect(const Vector3& v_in, const Vector3& n);

bool refract(const Vector3& v, const Vector3& n, float ni_over_nt, Vector3& refracted);

float schlick(float cosine, float ref_idx);

} // namespace renderer

#endif

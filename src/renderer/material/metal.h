#ifndef RENDERER_MATERIAL_METAL_H
#define RENDERER_MATERIAL_METAL_H

#include "renderer/material/material.h"

namespace renderer {

class Metal : public Material
{
  public:
	Metal(const Vector3& al, float fuzz);

	bool scatter(
        const Ray& ray_in,
        const HitRecord& hit_rec,
        Vector3& attenuation,
	    Ray& ray_scattered) const override;

	Vector3 albedo;
	float fuzz;
};

} // namespace renderer

#endif

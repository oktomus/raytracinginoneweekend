#include "renderer/material/lambertian.h"

using namespace foundation;

namespace renderer {

Lambertian::Lambertian(const Vector3 &al) :
    albedo(al)
{
}

bool Lambertian::scatter(
    const Ray &ray_in,
    const HitRecord &hit_rec,
    Vector3 &attenuation,
    Ray &ray_scattered) const
{
	Vector3 target = hit_rec.p + hit_rec.normal + random_in_unit_sphere();
	ray_scattered = Ray(hit_rec.p, target - hit_rec.p);
	attenuation = albedo;
	return true;
}

} // namespace renderer

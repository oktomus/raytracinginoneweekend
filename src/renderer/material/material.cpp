#include "renderer/material/material.h"

#include "foundation/math/vector3.h"

using namespace foundation;

namespace renderer {

Vector3 random_in_unit_sphere()
{
	Vector3 p;
	do
	{
		p = 2.0f * Vector3(drand48(), drand48(), drand48()) - Vector3(1, 1, 1);
	} while (p.squared_length() >= 1.0);
	return p;
}

Vector3 reflect(const Vector3& v_in, const Vector3& n)
{
    return v_in - 2 * Vector3::dot(v_in, n) * n;
}

bool refract(const Vector3& v, const Vector3& n, float ni_over_nt, Vector3& refracted)
{
    Vector3 uv = Vector3::normalize(v);
	float dt = Vector3::dot(uv, n);
	float disc = 1.0f - ni_over_nt * ni_over_nt * (1.0f - dt * dt);

	if (disc > 0)
	{
		refracted = ni_over_nt * (uv - n * dt) - n * sqrt(disc);
		return true;
	}
	return false;
}

float schlick(float cosine, float ref_idx)
{
    float r0 = (1.0f - ref_idx) / (1.0f + ref_idx);
	r0 = r0 * r0;
    return r0 + (1.0f - r0) * pow(1.0f - cosine, 5);
}

} // namespace renderer

#include "renderer/material/metal.h"

using namespace foundation;

namespace renderer {

Metal::Metal(const Vector3 &al, float fuzz) :
    albedo(al),
    fuzz(fuzz)
{
}

bool Metal::scatter(
    const Ray &ray_in,
    const HitRecord &hit_rec,
    Vector3 &attenuation,
    Ray &ray_scattered) const
{
	Vector3 reflected = reflect(Vector3::normalize(ray_in.direction()), hit_rec.normal);
	ray_scattered = Ray(hit_rec.p, reflected + fuzz * random_in_unit_sphere());
	attenuation = albedo;
	return (Vector3::dot(ray_scattered.direction(), hit_rec.normal) > 0);
}

} // namespace renderer

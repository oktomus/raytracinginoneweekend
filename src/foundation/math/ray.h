#ifndef FOUNDATION_MATH_RAY_H
#define FOUNDATION_MATH_RAY_H

#include "foundation/math/vector3.h"

namespace foundation {

class Ray
{
  public:
	Ray();
	Ray(const Vector3& origin, const Vector3& direction);

	const Vector3& origin() const;
	const Vector3& direction() const;

	Vector3 point_at_parameter(const float & t) const;

	Vector3 m_origin;
	Vector3 m_direction;

};

} // namespace foundation

#endif

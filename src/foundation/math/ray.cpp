#include "foundation/math/ray.h"

namespace foundation {

Ray::Ray() :
    Ray(Vector3(), Vector3())
{
}

Ray::Ray(const Vector3 &origin, const Vector3 &direction) :
    m_origin(origin), m_direction(direction)
{
}

const Vector3 & Ray::origin() const
{
	return m_origin;
}

const Vector3 & Ray::direction() const
{
	return m_direction;
}

Vector3 Ray::point_at_parameter(const float &t) const
{
	return m_origin + t * m_direction;
}

} // namespace foundation

#ifndef FOUNDATION_MATH_VECTOR3_H
#define FOUNDATION_MATH_VECTOR3_H

#include <cmath>
#include <cstdlib>
#include <iostream>

namespace foundation {

class Vector3
{
  public:
    Vector3();
    Vector3(const float& px, const float& py, const float& pz);

	float length() const;
	float squared_length() const;

	void normalize();

	static Vector3 normalize(Vector3 a);

	static float dot(const Vector3& a, const Vector3& b);

	static Vector3 cross(const Vector3 & a, const Vector3 & b);

	Vector3 & operator+=(const Vector3& b);
	Vector3 & operator+=(const float& b);
	Vector3 & operator-=(const Vector3& b);
	Vector3 & operator*=(const Vector3& b);
	Vector3 & operator*=(const float& b);
	Vector3 & operator/=(const float& b);

	friend Vector3 operator+ (Vector3 a, const Vector3& b);
	friend Vector3 operator+ (Vector3 a, const float& b);
	friend Vector3 operator- (Vector3 a, const Vector3& b);
	friend Vector3 operator- (Vector3 a);
	friend Vector3 operator* (Vector3 a, const Vector3& b);
	friend Vector3 operator* (Vector3 a, float b);
	friend Vector3 operator* (const float& a, Vector3 b);
	friend Vector3 operator/ (Vector3 a, float b);

	friend std::ostream & operator<<(std::ostream & os, const Vector3 & v);

    float x, y, z;
};

} // namespace foundation

#endif

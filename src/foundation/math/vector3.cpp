#include "foundation/math/vector3.h"

namespace foundation {

Vector3::Vector3() :
    x(0), y(0), z(0)
{
}

Vector3::Vector3(const float &px, const float &py, const float &pz) :
    x(px), y(py), z(pz)
{
}

float Vector3::length() const
{
	return sqrt(squared_length());
}

float Vector3::squared_length() const
{
	return x * x + y * y + z * z;
}

void Vector3::normalize()
{
	(*this) /= length();
}

Vector3 Vector3::normalize(Vector3 a)
{
	a.normalize();
	return a;
}

float Vector3::dot(const Vector3 &a, const Vector3 &b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}

Vector3 Vector3::cross(const Vector3 &a, const Vector3 &b)
{
	return Vector3(
        a.y * b.z - b.y * a.z,
        a.z * b.x - b.z * a.x,
        a.x * b.y - b.x * a.y
    );
}

Vector3 &Vector3::operator+=(const Vector3 & b)
{
	x += b.x; y += b.y; z += b.z;
	return *this;
}

Vector3 &Vector3::operator+=(const float & b)
{
	x += b; y += b; z += b;
	return *this;
}

Vector3 &Vector3::operator-=(const Vector3 & b)
{
	x -= b.x; y -= b.y; z -= b.z;
	return *this;
}

Vector3 &Vector3::operator*=(const Vector3 & b)
{
	x *= b.x; y *= b.y; z *= b.z;
	return *this;
}

Vector3 &Vector3::operator*=(const float & b)
{
	x *= b; y *= b; z *= b;
	return *this;
}

Vector3 &Vector3::operator/=(const float & b)
{
	x /= b; y /= b; z /= b;
	return * this;
}

Vector3 operator+(Vector3 a, const Vector3& b)
{
	a +=  b;
	return a;
}

Vector3 operator+(Vector3 a, const float& b)
{
	a +=  b;
	return a;
}

Vector3 operator-(Vector3 a, const Vector3& b)
{
	a -=  b;
	return a;
}

Vector3 operator-(Vector3 a)
{
	a.x = -a.x; a.y = -a.y; a.z = -a.z;
	return a;
}

Vector3 operator*(Vector3 a, const Vector3& b)
{
	a *=  b;
	return a;
}

Vector3 operator*(Vector3 a, float b)
{
	a *=  b;
	return a;
}

Vector3 operator*(const float& a, Vector3 b)
{
	b *=  a;
	return b;
}

Vector3 operator/(Vector3 a, float b)
{
	a /=  b;
	return a;
}

std::ostream & operator<<(std::ostream & os, const Vector3 & v)
{
	os << v.x << " " << v.y << " " << v.z;
	return os;
}

} // namespace foundation


#include "foundation/math/vector3.h"
#include "foundation/math/ray.h"

#include "renderer/camera/camera.h"
#include "renderer/material/material.h"
#include "renderer/material/dielectric.h"
#include "renderer/material/lambertian.h"
#include "renderer/material/metal.h"
#include "renderer/object/hitable.h"
#include "renderer/object/sphere.h"

#include <cfloat>
#include <cmath>
#include <iostream>

using namespace foundation;
using namespace renderer;

class HitableList : public Hitable
{
  public:
	HitableList() {}
	HitableList(Hitable **l, int n) { m_list = l; m_list_size = n; }

	bool hit(const foundation::Ray& r, float t_min, float t_max, HitRecord & rec) const
	{
		HitRecord temp_rec;
		bool hit_any = false;
		double closest = t_max;

		for(int i = m_list_size - 1; i >= 0; --i)
		{
			if (m_list[i]->hit(r, t_min, closest, temp_rec))
			{
                hit_any = true;
                closest = temp_rec.t;
                rec = temp_rec;
			}
		}

		return hit_any;
	}

  private:

	Hitable ** m_list;
	int m_list_size;
};

foundation::Vector3 do_color(const foundation::Ray & ray, Hitable * world, int depth)
{
	HitRecord rec;

	if (world->hit(ray, 0.001f, FLT_MAX, rec))
	{
		Ray scattered;
		Vector3 attenuation;

		if (depth < 50 && rec.mat_ptr->scatter(ray, rec, attenuation, scattered))
		{
			return attenuation * do_color(scattered, world, depth + 1);
		}
		else
		{
			return Vector3(0, 0, 0);
		}
	}

	// Background color
	foundation::Vector3 unit = foundation::Vector3::normalize(ray.direction());
	float t = 0.5f * (unit.y + 1.0f);
	return (1.0f - t) * foundation::Vector3(1, 1, 1) + t * foundation::Vector3(0.5f, 0.7f, 1.0f);
}

Hitable * random_scene()
{
	int n = 500;
	Hitable **list = new Hitable*[n + 1];
	list[0] = new Sphere(Vector3(0, -1000, 0), 1000, new Lambertian(Vector3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++)
	{
		for (int b = -11; b < 11; b++)
		{
			Vector3 center(a + 0.9 * drand48(), 0.2, b + 0.9 * drand48());

			if ((center - Vector3(4, 0.2, 0)).length() > 0.9)
			{
				float choose_mat = drand48();
				if (choose_mat < 0.8) // Diffuse
				{
					list[i++] = new Sphere(center,0.2, new Lambertian(
						Vector3(drand48() * drand48(), drand48() * drand48(), drand48() * drand48())));
				}
				else if (choose_mat < 0.95) // Metal
				{
					list[i++] = new Sphere(center,0.2, new Metal(
						Vector3(0.5 * (1 + drand48()), 0.5 * (1 + drand48()), 0.5 * (1 + drand48())), 0.5 * drand48()));
				}
				else // Glass
				{
					list[i++] = new Sphere(center,0.2, new Dielectric(1.5));
				}
			}
		}
	}

	list[i++] = new Sphere(Vector3(0, 1, 0), 1, new Dielectric(1.5));
	list[i++] = new Sphere(Vector3(-4, 1, 0), 1, new Lambertian(Vector3(0.4, 0.2, 0.1)));
	list[i++] = new Sphere(Vector3(4, 1, 0), 1, new Metal(Vector3(0.7, 0.6, 0.5), 0.0));

	return new HitableList(list, i);
}

int main(int argc, const char * argv[])
{
	int size_x = 200,
        size_y = 100,
	    size_s = 100;

	std::cout << "P3\n" << size_x << " " << size_y << "\n255\n";

	foundation::Vector3 lower_left(-2.0f, -1.0f, -1.0f);
	foundation::Vector3 horizontal(4.0f, 0, 0);
	foundation::Vector3 vertical(0, 2, 0);
	foundation::Vector3 origin;

	Hitable * world = random_scene();

	Vector3 look_from(3, 3, 2);
	Vector3 look_at(0, 0, -1);
	float dist_to_focus = (look_from - look_at).length();
	float apperture = 0.1;
    renderer::Camera camera(
    	look_from,
    	look_at,
        Vector3(0, 1, 0),
        60,
        float(size_x) / float(size_y),
        apperture,
        dist_to_focus);

	for(int y = size_y - 1; y >= 0; --y)
	{
		for(int x = 0; x < size_x; ++x)
		{
			foundation::Vector3 color;
			for(int s = 0; s < size_s; ++s)
			{
                float u = float(x + drand48()) / float(size_x),
                    v = float(y + drand48()) / float(size_y);
                foundation::Ray r = camera.get_ray(u, v);
				color += do_color(r, world, 0);
			}
			color /= float(size_s);
			color = Vector3(sqrt(color.x), sqrt(color.y), sqrt(color.z));

			int pr = int(255.99f * color.x);
			int pg = int(255.99f * color.y);
			int pb = int(255.99f * color.z);
			std::cout << pr << " " << pg << " " << pb << "\n";
		}
	}

	std::cout << std::endl;

	return 0;
}
